function calcularMedia () {
    
    let notaUm = document.getElementById("notaUm").value;
    let notaDois = document.getElementById("notaDois").value;
    let notaTres = document.getElementById("notaTres").value;

    let media = (parseFloat(notaUm) + parseFloat(notaDois) + parseFloat(notaTres))/3;

    document.getElementById("media").innerHTML = media.toFixed(2);
    document.getElementById("situacao").innerHTML = media>=6?"Aprovado":"Reprovado";

}