const booksByCategory = 
[
    {
    category: "Riqueza",
    books: [
        {
        title: "Os segredos da mente milionária",
        author: "T. Harv Eker",
        },
        {
        title: "O homem mais rico da Babilônia",
        author: "George S. Clason",
        },
        {
        title: "Pai rico, pai pobre",
        author: "Robert T. Kiyosaki e Sharon L. Lechter",
        },
    ],
    },
    {
    category: "Inteligência Emocional",
    books: [
        {
        title: "Você é Insubstituível",
        author: "Augusto Cury",
        },
        {
        title: "Ansiedade – Como enfrentar o mal do século",
        author: "Augusto Cury",
        },
        {
        title: "Os 7 hábitos das pessoas altamente eficazes",
        author: "Stephen R. Covey"
        }
    ]
    }
];    

function countCategoryAndBooksOfEachCategory(booksByCategory){
    console.log(`Quantidade de categorias: ${booksByCategory.length}`);
    console.log('-------------------------------------')
    for (const category of booksByCategory) {
        console.log(`Categoria: ${category.category}`);
        console.log(`Quantidades de livros nesta categoria: ${category.books.length}`)
        console.log('-------------------------------------')
    }
}

function removeDuplicate(array){
    const arrayUniqueValues = [];
    for (let i = 0; i < array.length; i++) {
        if (arrayUniqueValues.indexOf(array[i]) === -1) 
        {
            arrayUniqueValues.push(array[i]);
        }
    }
    return arrayUniqueValues;
}

function uniqueAuthors(booksByCategory) {
    const authors = new Array();
    for (const category of booksByCategory) 
    {
        for (const books of category.books)
        {
            authors.push(books.author);
        }
    }
    return removeDuplicate(authors);
}

function booksOf(author){
    const books = new Array();
    for(let i = 0 ; i < booksByCategory.length ; i++) 
    {
        for (let j = 0 ; j < booksByCategory[i].books.length; j++)
        {
            if(author === booksByCategory[i].books[j].author) books.push(booksByCategory[i].books[j].title);
        }
    }
    return books;
}

//countCategoryAndBooksOfEachCategory(booksByCategory);

//let uniqueAuthors = uniqueAuthors(booksByCategory).length;
//console.log(uniqueAuthors);

//let authorBooks = booksOf("Augusto Cury");
//console.log(authorBooks);