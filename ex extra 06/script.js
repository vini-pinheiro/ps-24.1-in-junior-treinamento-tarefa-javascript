const registroFinanceiro = {
    receitas: [100, 250], 
    despesas: [300, 90]
}

function somarValoresDoArray(array){
    let soma = 0;
    for(let i = 0; i< array.length ; i++) soma += array[i];
    return soma;
}
function balancoFinanceiro(registroFinanceiro) {
    const totalReceitas = somarValoresDoArray(registroFinanceiro.receitas);
    const totalDespesas = somarValoresDoArray(registroFinanceiro.despesas);
    const saldo = totalReceitas - totalDespesas;
    console.log(`Total de Receitas: ${totalReceitas}`)
    console.log(`Total de Despesas: ${totalDespesas}`)
    console.log(`---------------------------`)
    console.log(`Saldo: ${saldo}`)
    return saldo;
}

let saldoDaFamilia = balancoFinanceiro(registroFinanceiro);