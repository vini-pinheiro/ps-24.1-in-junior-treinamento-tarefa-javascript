function converterParaCelsius() {
    let valorFahrenheit = document.getElementById("fahrenheit").value;

    let valorCelsius = (5 * (valorFahrenheit - 32)) / 9;

    let mensagem = "O valor em Celsius é: " + valorCelsius.toFixed(2) + "°C";

    console.log(mensagem);
}