function criarMatriz (linha, coluna) {

    let matriz = [];
    
    for (let i=0; i< linha; i++) {
        matriz[i] = [];
        for (let j=0; j< coluna; j++)  {
            matriz[i][j] = 0;
        }
    }

    return matriz;
}

function imprimirMatriz(matriz) {

    if(matriz == null) return console.log("Não é possível multiplicar as matrizes. O número de colunas de A deve ser igual ao número de linhas de B.");

    let linha = matriz.length, coluna = matriz[0].length;

    for (let i=0; i< linha; i++) {
        for (let j=0; j< coluna; j++)  {
            console.log("Matriz Resultante [%i|%i]" , i, j, " = " , matriz[i][j], "\n");
        }
    }
}

function multiplicacaoMatrizes(matrizA, matrizB) {

    let linhasA  = matrizA.length;
    let colunasA = matrizA[0].length;

    let linhasB  = matrizB.length;
    let colunasB = matrizB[0].length;
    
    if(colunasA != linhasB) {
        return null;
    }

    let matrizResultante = criarMatriz(linhasA, colunasB);

    for (let i=0; i < linhasA; i++) {
        for (let j=0; j < colunasB; j++)  {
            for (let k=0; k < colunasA; k++) {
                matrizResultante[i][j] += matrizA[i][k] * matrizB[k][j];
            }
        }
    }

    return matrizResultante;

}


let matrizA = [ 
                [2,-1], 
                [2, 0]
               ];
let matrizB =  [ 
                    [ 2,3],
                    [-2,1] 
                  ];

let resultante = multiplicacaoMatrizes(matrizA, matrizB);

imprimirMatriz(resultante);